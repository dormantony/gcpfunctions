from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
import pickle
import numpy as np

def run():
    x = pickle.load(open('data/data_x', 'rb'))
    y = pickle.load(open('data/data_y', 'rb'))
    print(x.shape)
    print(y.shape)
    train_features, test_features, train_labels, test_labels = train_test_split(x, y, test_size=0.25, random_state=12)
    print('Training Features Shape:', train_features.shape)
    print('Training Labels Shape:', train_labels.shape)
    print('Testing Features Shape:', test_features.shape)
    print('Testing Labels Shape:', test_labels.shape)
    # Instantiate model with 1000 decision trees
    rf = RandomForestRegressor(n_estimators=1000, random_state=42)
    # Train the model on training data
    rf.fit(train_features, train_labels)
    # Use the forest's predict method on the test data
    predictions = rf.predict(test_features)
    # Calculate the absolute errors
    errors = abs(predictions - test_labels)
    # Print out the mean absolute error (mae)
    print('Mean Absolute Error:', round(np.mean(errors), 2), 'minutes.')
    filename = 'finalized_model.sav'
    pickle.dump(rf, open(filename, 'wb'))
    print('guardado')
    #
    print('cargando')
    loaded_model = pickle.load(open(filename, 'rb'))
    print('cargado')
    result =loaded_model.predict(test_features)
    print(result)




if __name__ == '__main__':
    run()
