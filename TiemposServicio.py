import pandas as pd
import sklearn.ensemble.forest
import pickle


def procesar():
    new_preds = pd.read_csv('data/apredecir.csv', header=0)
    new_preds = new_preds.drop(['Unnamed: 0'], axis=1)
    print('star loading model')
    print('end loading model')
    pic = pickle.load(open("finalized_model.sav", "rb"))
    new_predictions = pic.predict(new_preds)
    preds = pd.DataFrame(new_predictions)
    print(preds.head())



if __name__ == '__main__':
    procesar()